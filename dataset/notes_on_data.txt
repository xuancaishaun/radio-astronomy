Notes:
1. Replace ',' with '-' in .xlxs
2. Avoid '.' in latitudes and longitudes
3. For DR, mark 'Purpose' as Number type when importing it to 
   Matlab using Dialog box, before running import_data()