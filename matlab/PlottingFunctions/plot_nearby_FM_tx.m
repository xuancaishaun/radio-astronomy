% This function plots Tx stations near MWA (-26.7020, 116.6722),
% with a range specified by R.

%% Loading
if exist('../dataset/data/data_2016_Mar_10.mat', 'file') == 2
    load('../dataset/data/data_2016_Mar_10.mat');
else
    disp('No dataset is found; please generate with import_data()');
end

if ~exist('MWA_lat', 'var') || ~exist('MWA_lon', 'var')
    error('Cannot find MWA coordinates');
end

%% Processing
% Save nearby station data to file if SAVE_DAVE is TRUE
SAVE_DATA = false;
DATA_FILE_NAME = '../dataset/data/nearby_tx.mat';

R = 1000; % km
Freq_Range = [80, 130];     %AM/FM
%Freq_Range = [230, 1000];    % DTV

dataset_num = 4;

stations = cell(dataset_num,1);
stations(1) = {AM};
stations(2) = {FM};
stations(3) = {DR};
stations(4) = {TV};

idx = cell(dataset_num,1);        % Holding idx of nearby stations
loc = cell(dataset_num,1);        % Lat and lon of nearby stations
freq = cell(dataset_num,1);      % Operating freq of nearby stations
dist = cell(dataset_num,1);

for i=1:dataset_num
   tmp_stations = stations{i}; % AM/FM/DR/TV
   idx{i} = [];
   loc{i} = [];
   freq{i} = [];
   dist{i} = [];
   
   N = length(tmp_stations);
   
   for j=1:N
        tx = tmp_stations{j};
        dist_km = compute_dist(MWA_lat, MWA_lon, tx.Lat, tx.Lon);

        if (dist_km<R) && (tx.FreqMHz>=Freq_Range(1)) && (tx.FreqMHz<=Freq_Range(2))
            idx{i} = [idx{i}; j];
            loc{i} = [loc{i}; [tx.Lat, tx.Lon]];
            freq{i} = [freq{i}; tx.FreqMHz];
            dist{i} = [dist{i}; dist_km];
        end
   end
end

%% Print out closest out-of-band FM station
[min_dist, min_idx] = min(dist{2});
fprintf('Closest FM tx is at %s with a dist. of %f\n', ...
        mat2str(loc{2}(min_idx, :)), min_dist);
    
%% Print out closest out-of-band DTV station
[min_dist, min_idx] = min(dist{4});
fprintf('Closest DTV tx is at %s with a dist. of %f\n', ...
        mat2str(loc{4}(min_idx, :)), min_dist);

%% Plot nearby AM/FM stations on the map
figure1 = figure;
axes1 = axes('Parent',figure1,'FontSize',20);
scatter(MWA_lon, MWA_lat, 'filled', 'r', 'SizeData', 100, ...
        'DisplayName', 'WMA');  % MWA 
hold on;
%scatter(loc{1}(:,2), loc{1}(:,1), 'filled', 'blue', 'd', 'SizeData', 100,...
%        'DisplayName', sprintf('AM - %d', length(loc{1}(:,1)))); % AM
scatter(loc{2}(:,2), loc{2}(:,1), 'fill', 'b',...%'red', '*', 'SizeData', 150,...
        'DisplayName', sprintf('FM - %d', length(loc{2}(:,1)))); % FM
%scatter(loc{3}(:,2), loc{3}(:,1), 'filled', 'd', 'SizeData', 100); % DR
%scatter(loc{4}(:,2), loc{4}(:,1), 'filled', 'black', 'o', 'SizeData', 100, ...
%        'DisplayName', sprintf('DTV - %d', length(loc{4}(:,1)))); % TV
title(sprintf('FM stations with %dkm', R), 'FontSize', 20);
xlabel('Longitude', 'FontSize', 30);
ylabel('Latitude', 'FontSize', 30);

axis equal;

xlim([min(loc{2}(:,2))-4, max(loc{2}(:,2))+4]);
ylim([min(loc{2}(:,1))-4, max(loc{2}(:,1))+4]);
% legend('WMA', ...%sprintf('AM - %d', length(loc{1}(:,1))), ...
%     sprintf('FM - %d', length(loc{2}(:,1))), ...
%     sprintf('DTV - %d', length(loc{4}(:,1))));
legend show;
grid on;
hold off;

%% Plot out-of-band DTV stations on the map
figure1 = figure;
axes1 = axes('Parent',figure1,'FontSize',20);
scatter(MWA_lon, MWA_lat, 'filled', 'r', 'SizeData', 100, ...
        'DisplayName', 'WMA');  % MWA 
hold on;
scatter(loc{4}(:,2), loc{4}(:,1), 'filled', 'black', 'o', 'SizeData', 100, ...
        'DisplayName', sprintf('DTV - %d', length(loc{4}(:,1)))); % TV
title(sprintf('Stations with 400km near WMA (> 230 MHz)'), 'FontSize', 20);
xlabel('Longitude', 'FontSize', 30);
ylabel('Latitude', 'FontSize', 30);
xlim([MWA_lon-4, MWA_lon+4]);
ylim([MWA_lat-4, MWA_lat+4]);
legend show;
grid on;
hold off;

%% Plot histogram of stations in the band of 0-130 MHz
%edges = 174.5:7:230;
edges = 0:1:130;

y1 = hist(freq{1}, edges);
y2 = hist(freq{2}, edges);

%figure2 = figure;
bar(edges, [y1;y2]','stacked');

xlim([0, 130]);
xlabel('Freq (MHz)', 'FontSize', 30);
ylabel('Counts', 'FontSize', 30);
title('# of stations operating in 0-130MHz', 'FontSize', 20);
legend('AM', 'FM');
%legend('DTV');

%% Plot histogram of stations in the band of 0-130 MHz
hist(freq{4});

xlabel('Freq (MHz)', 'FontSize', 30);
ylabel('Counts', 'FontSize', 30);
title('# of stations operating > 230 MHz', 'FontSize', 20);
legend('DTV');






