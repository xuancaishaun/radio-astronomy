% This function plots Tx stations near MWA (-26.7020, 116.6722),
% with a range specified by R.

%% Loading
if exist('../dataset/data/data_2016_Mar_10.mat', 'file') == 2
    load('../dataset/data/data_2016_Mar_10.mat');
else
    disp('No dataset is found; please generate with import_data()');
end

if ~exist('MWA_lat', 'var') || ~exist('MWA_lon', 'var')
    error('Cannot find MWA coordinates');
end

%% Processing
% Save nearby station data to file if SAVE_DAVE is TRUE
SAVE_DATA = false;
DATA_FILE_NAME = '../dataset/data/nearby_tx.mat';

R = 1000; % km
%Freq_Range = [165, 195];
Freq_Range = [139, 197];

dataset_num = 4;

stations = cell(dataset_num,1);
stations(1) = {AM};
stations(2) = {FM};
stations(3) = {DR};
stations(4) = {TV};

idx = cell(dataset_num,1);        % Holding idx of nearby stations
loc = cell(dataset_num,1);        % Lat and lon of nearby stations
freq = cell(dataset_num,1);      % Operating freq of nearby stations
dist = cell(dataset_num,1);

for i=1:dataset_num
   tmp_stations = stations{i}; % AM/FM/DR/TV
   idx{i} = [];
   loc{i} = [];
   freq{i} = [];
   dist{i} = [];
   
   N = length(tmp_stations);
   
   for j=1:N
        tx = tmp_stations{j};
        dist_km = compute_dist(MWA_lat, MWA_lon, tx.Lat, tx.Lon);

        if (dist_km<R) && (tx.FreqMHz>=Freq_Range(1)) && (tx.FreqMHz<=Freq_Range(2))
            idx{i} = [idx{i}; j];
            loc{i} = [loc{i}; [tx.Lat, tx.Lon]];
            freq{i} = [freq{i}; tx.FreqMHz];
            dist{i} = [dist{i}; dist_km];
        end
   end
end

% Print DTV channel IDs
for i=1:length(freq{4})
    fprintf('%s, ', get_channel_id(freq{4}(i), 'TV'));
end

fprintf('\n');

for i=1:length(freq{4})
    chn_ID = get_channel_id(freq{4}(i), 'TV');
    if strcmp(chn_ID, '9') || strcmp(chn_ID, '9A') || freq{4}(i) == 198.625
        fprintf('CH %s at %s, dist: %f, idx: %d\n', ...
            chn_ID, mat2str(loc{4}(i,:)), dist{4}(i), idx{4}(i));
    else
        fprintf('%s: %f, idx: %d, Ant: %s, power: %.3f kW\n', ...
                mat2str(loc{4}(i,:)), dist{4}(i), idx{4}(i), TV{idx{4}(i)}.AntPattern, TV{idx{4}(i)}.MaxERPW/1000);
    end
end

%% Plot nearby DTV stations on the map
figure1 = figure;
axes1 = axes('Parent',figure1,'FontSize',20);
scatter(MWA_lon, MWA_lat, 'filled', 'r', 'SizeData', 100, ...
        'DisplayName', 'WMA');  % MWA 
hold on;
%scatter(loc{1}(:,2), loc{1}(:,1), 'filled', 'blue', 'd', 'SizeData', 100); % AM
%scatter(loc{2}(:,2), loc{2}(:,1), 'red', 'x', 'SizeData', 150); % FM
%scatter(loc{3}(:,2), loc{3}(:,1), 'filled', 'd', 'SizeData', 100); % DR
scatter(loc{4}(:,2), loc{4}(:,1), 'filled', 'black', 'o', 'SizeData', 100, ...
        'DisplayName', sprintf('DTV - %d', length(loc{4}(:,1)))); % TV
title(sprintf('Stations within %dkm (%d-%dMHz)', ...
      R, Freq_Range(1), Freq_Range(2)), 'FontSize', 20);
xlabel('Longitude', 'FontSize', 30);
ylabel('Latitude', 'FontSize', 30);
axis equal
xlim([min(loc{4}(:,2))-6, max(loc{4}(:,2))+6]);
ylim([min(loc{4}(:,1))-2, max(loc{4}(:,1))+2]);
% legend('WMA', ...%sprintf('AM - %d', length(loc{1}(:,1))), ...
%     sprintf('FM - %d', length(loc{2}(:,1))), ...
%     sprintf('DTV - %d', length(loc{4}(:,1))));
legend show;
grid on;
hold off;

%% Plot histogram of stations in the band of 130-230 MHz
edges = 174.5:7:230;
%y2 = hist(freq{2}, edges);
y4 = hist(freq{4}, edges);

%figure2 = figure;
%bar(edges, [y2;y4]','stacked');
bar(edges, y4');

xlabel('Freq (MHz)', 'FontSize', 15);
ylabel('Counts', 'FontSize', 15);
title('# of stations operating in 130 - 230MHz', 'FontSize', 15);
%legend('FM', 'DTV');
legend('DTV');






