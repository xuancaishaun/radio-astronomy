% This is the main function

% General execution sequence:
%   - Run set_path.m to set path and add folders
%   - Run import_data.m to import data

% Plotting functions:
%   - plot_nearby_tx.m: plot WMA and nearby Tx stations

%% ==================================================================
%             Calculate RFI from DTV to MWA in 165-195 MHz
%  ==================================================================
% RFI: co-channel, adjacent-channel 
import_data();

%band = [165, 195+7];    % Primarily CH 6,7,8; also include CH9 for adj-chn interference
band = [165, 195]; 
max_dist = 1000;        % in km
MWA = [MWA_lat, MWA_lon];

% Filter DTV Tx by dist
nearby_tx_cell = [];

N = length(TV);

for i=1:N
    tx = TV{i};
    dist_km = compute_dist(MWA(1), MWA(2), tx.Lat, tx.Lon);
    
    nearby_tx = [];
    
    if (dist_km < max_dist) && (tx.FreqMHz >= band(1)) && (tx.FreqMHz <= band(2))
        nearby_tx.Idx = i;
        nearby_tx.Loc = [tx.Lat, tx.Lon];
        nearby_tx.FreqMHz = tx.FreqMHz;
        nearby_tx.DistKm = dist_km;
        nearby_tx.ChnId = get_channel_id(tx.FreqMHz, 'TV');
        nearby_tx_cell = [nearby_tx_cell, {nearby_tx}];
        fprintf('%s, ', nearby_tx.ChnId);
    end
end
fprintf('\n');

%% There are only CH 6,7,8 in 165-195 MHz (for co-channel interference)
models = {'FS', 'TR', 'LRP2P', 'LRArea'};           
% FS: Free-Space                       
% TR: Two-Ray                        
% LRP2P: LR Point-to-Point mode                        
% LRArea: LR Area mode

chn_table = {'6', '7', '8', '9'};
co_chn_I = zeros(length(chn_table), length(models));
co_chn_I_dBu = zeros(length(chn_table), length(models));

for k = 1:length(models)
    model = models{k};
    fprintf('Compute co-channel interferce for model: %s\n', model);
    
    for i=1:length(nearby_tx_cell)
        nearby_tx = nearby_tx_cell{i};
        rss_dBu = compute_dtv_rss(TV{nearby_tx.Idx}, MWA, model);
        fprintf('CH %s, MaxERPW: %f, DistKm: %f, RSS at MWA = %f\n', ...
                 nearby_tx.ChnId, TV{nearby_tx.Idx}.MaxERPW, nearby_tx.DistKm, rss_dBu);

        chn_idx = 0;
        for j=1:length(chn_table)
            if strcmp(nearby_tx.ChnId, chn_table{j})
                chn_idx = j;
            end
        end

        if chn_idx > 0
            % Caution: the relationshop between dBuV/m and uV/m is 
            % dBuV/m = 20*log10(uV/m)
            co_chn_I(chn_idx, k) = co_chn_I(chn_idx, k) + 10^(rss_dBu/20);
        end
    end


    for ii=1:length(co_chn_I_dBu)
        co_chn_I_dBu(ii, k) = 20*log10(co_chn_I(ii, k));   % in dBuV/m
    end
end

%% Here we compute RFI for each DTV station individually. We do not really care about CH ID.
models = {'FS', 'TR', 'LRP2P', 'LRArea'};  
co_chn_I_dBu_for_each_tx = zeros(length(nearby_tx_cell), length(models));

for i=1:length(nearby_tx_cell)
    nearby_tx = nearby_tx_cell{i};
    
    chn_idx = str2num(nearby_tx.ChnId);
    
    if (chn_idx>=6) && (chn_idx<=8)
        for j=1:length(models)
            model = models{j};

            rss_dBu = compute_dtv_rss(TV{nearby_tx.Idx}, MWA, model);
            fprintf('CH %s, MaxERPW: %f, DistKm: %f, RSS at MWA = %f\n', ...
                     nearby_tx.ChnId, TV{nearby_tx.Idx}.MaxERPW, nearby_tx.DistKm, rss_dBu);

            co_chn_I_dBu_for_each_tx(i,j) = rss_dBu;
        end
    end
end

%%
YY = co_chn_I_dBu_for_each_tx;
for i=1:length(YY(:,1))
    for j=1:length(YY(1,:))
        YY(i,j) = dBuV_to_Jy(YY(i,j), 'DTV');
    end
end

fprintf('------------------ Co-Chn Interference ------------------\n');
fprintf('Model        FS        TR        LRP2P        LRArea\n');
fprintf('Min      %8.3f   %8.3f   %8.3f    %8.3f\n', ...
         min(YY(:,1)), min(YY(:,2)), min(YY(:,3)), min(YY(:,4)))
fprintf('Med      %8.3f   %8.3f   %8.3f    %8.3f\n', ...
         median(YY(:,1)), median(YY(:,2)), median(YY(:,3)), median(YY(:,4)))
fprintf('Max      %8.3f   %8.3f   %8.3f    %8.3f\n', ...
         max(YY(:,1)), max(YY(:,2)), max(YY(:,3)), max(YY(:,4)))

%%
fprintf('------------------ Co-Chn Interference ------------------\n');
fprintf('Model       CH6       CH7       CH8      (CH9)  in dBuV/m\n');
for k = 1:length(models)
    fprintf('%6s, %8.3f, %8.3f, %8.3f, %8.3f\n', models{k}, ...
             co_chn_I_dBu(1,k), co_chn_I_dBu(2,k), co_chn_I_dBu(3,k), co_chn_I_dBu(4,k))
end
fprintf('---------------------------------------------------------\n');

%% CH 9 may have power leakge into CH 8
% adj_chn_I = zeros(3, length(models)); % CH 6,7,8
% 
% for i=1:length(models)
%     adj_chn_I(1, i) = co_chn_I(2, i)*1.06*10^(-4);
%     adj_chn_I(2, i) = (co_chn_I(1, i) + co_chn_I(3, i) )*1.06*10^(-4);
%     adj_chn_I(3, i) = (co_chn_I(2, i) + co_chn_I(4, i) )*1.06*10^(-4);
% end
% 
% adj_chn_I_dB = 10*log10(adj_chn_I);

%fprintf('Adj-channel interference (dBuV/m) = \n%s\n', mat2str(adj_chn_I_dB));

%%
% fprintf('----------------- Adj-Chn Interference ------------------\n');
% fprintf('Model       CH6       CH7       CH8  in dBuV/m\n');
% for k = 1:length(models)
%     fprintf('%6s, %8.3f, %8.3f, %8.3f\n', models{k}, ...
%              adj_chn_I_dB(1,k), adj_chn_I_dB(2,k), adj_chn_I_dB(3,k))
% end
% fprintf('---------------------------------------------------------\n');

%% Plot co-chn interference for different models
bar(co_chn_I_dBu(1:3, :));


%% Plot 
ymatrix1 = [co_chn_I_dBu(1:3, 1), adj_chn_I_dB(1:3, 1)];

figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1,'YGrid','on','XTickLabel',{'6','7','8'},...
    'XTick',[1 2 3],...
    'FontSize',30);
box(axes1,'on');
hold(axes1,'all');

ylim(axes1,[0 90]);

% Create multiple lines using matrix input to bar
bar1 = bar(ymatrix1,'Parent',axes1);
set(bar1(1),'DisplayName','co-chn');
set(bar1(2),'DisplayName','adj-chn');

% Create xlabel
xlabel('CH ID','FontSize',30);

% Create ylabel
ylabel('RFI (dBuV/m)','FontSize',30);

% Create legend
legend1 = legend(axes1,'show');
set(legend1,'FontSize',20);
