% This is the main function

% General execution sequence:
%   - Run set_path.m to set path and add folders
%   - Run import_data.m to import data

% Plotting functions:
%   - plot_nearby_tx.m: plot WMA and nearby Tx stations

%% ==================================================================
%             Calculate RFI from FM to MWA in 88-108 MHz
%  ==================================================================
% RFI: co-channel
import_data();

band = [88, 108]; 
max_dist = 1000;        % in km
MWA = [MWA_lat, MWA_lon];

% Filter FM Tx by dist
nearby_tx_cell = [];

N = length(FM);

for i=1:N
    tx = FM{i};
    dist_km = compute_dist(MWA(1), MWA(2), tx.Lat, tx.Lon);
    
    nearby_tx = [];
    
    if (dist_km < max_dist) && (tx.FreqMHz >= band(1)) && (tx.FreqMHz <= band(2))
        nearby_tx.Idx = i;
        nearby_tx.Loc = [tx.Lat, tx.Lon];
        nearby_tx.FreqMHz = tx.FreqMHz;
        nearby_tx.DistKm = dist_km;
        nearby_tx.ChnId = get_channel_id(tx.FreqMHz, 'FM');
        nearby_tx_cell = [nearby_tx_cell, {nearby_tx}];
        fprintf('%s, ', nearby_tx.ChnId);
    end
end
fprintf('\n');

%% Aggregated co-channel FM RFI for each channel (CH 1-52)
models = {'FS', 'TR', 'LRP2P', 'LRArea'};           
% FS: Free-Space                       
% TR: Two-Ray                        
% LRP2P: LR Point-to-Point mode                        
% LRArea: LR Area mode

%freq_table = 88.1:0.2:107.9;
freq_table = 88.1:0.2:98.5;
chn_id_table = 1:length(freq_table);
co_chn_I = zeros(length(chn_id_table), length(models));
co_chn_I_dBu = zeros(length(chn_id_table), length(models));


for k = 1:length(models)
    model = models{k};
    fprintf('Compute co-channel interferce for model: %s\n', model);
    
    for i=1:length(nearby_tx_cell)
        nearby_tx = nearby_tx_cell{i};
        
        if (nearby_tx.FreqMHz >= min(freq_table)) && (nearby_tx.FreqMHz <= max(freq_table))
            rss_dBu = compute_dtv_rss(FM{nearby_tx.Idx}, MWA, model);
            fprintf('CH %s, MaxERPW: %f, DistKm: %f, RSS at MWA = %f\n', ...
                     nearby_tx.ChnId, FM{nearby_tx.Idx}.MaxERPW, nearby_tx.DistKm, rss_dBu);

            chn_idx = str2num(nearby_tx.ChnId);
            % Caution: the relationshop between dBuV/m and uV/m is 
            % dBuV/m = 20*log10(uV/m)
            co_chn_I(chn_idx, k) = co_chn_I(chn_idx, k) + 10^(rss_dBu/20);
        end 
    end


    for ii=1:length(co_chn_I_dBu)
        co_chn_I_dBu(ii, k) = 20*log10(co_chn_I(ii, k));   % in dBuV/m
    end
end

%%
co_chn_FM_HD_RFI_Jy = co_chn_I_dBu;

for i=1:length(co_chn_FM_HD_RFI_Jy(:,1))
    for j=1:length(co_chn_FM_HD_RFI_Jy(1,:))
        co_chn_FM_HD_RFI_Jy(i,j) = dBuV_to_Jy(co_chn_FM_HD_RFI_Jy(i,j), 'FM-HD');
    end
end

median(co_chn_I_dBu)
max(co_chn_I_dBu)
median(co_chn_FM_HD_RFI_Jy)
max(co_chn_FM_HD_RFI_Jy)

%% Compute co-channel harmonically distorted FM RFI for each station in CH 1-47
models = {'FS', 'TR', 'LRP2P', 'LRArea'};  
count = 0;
max_chn_id = 52;

for i=1:length(nearby_tx_cell)
    chn_idx = str2num(nearby_tx_cell{i}.ChnId);
    if (chn_idx>=1) && (chn_idx<=max_chn_id)
        count = count + 1;
    end
end


co_chn_I_dBu_for_each_tx = zeros(count, length(models));

count = 0;
for i=1:length(nearby_tx_cell)
    nearby_tx = nearby_tx_cell{i};
    chn_idx = str2num(nearby_tx.ChnId);
    
    if (chn_idx>=1) && (chn_idx<=max_chn_id)
        count = count + 1;
        
        for j=1:length(models)
            model = models{j};

            rss_dBu = compute_dtv_rss(FM{nearby_tx.Idx}, MWA, model);
            fprintf('CH %s, MaxERPW: %f, DistKm: %f, RSS at MWA = %f\n', ...
                     nearby_tx.ChnId, FM{nearby_tx.Idx}.MaxERPW, nearby_tx.DistKm, rss_dBu);

            co_chn_I_dBu_for_each_tx(count,j) = rss_dBu;
        end
    end
end

%%
YY = co_chn_I_dBu_for_each_tx;
for i=1:length(YY(:,1))
    for j=1:length(YY(1,:))
        YY(i,j) = dBuV_to_Jy(YY(i,j), 'FM-HD');
    end
end

fprintf('------------------ Co-Chn RFI with HD ------------------\n');
fprintf('Model        FS        TR        LRP2P        LRArea\n');
fprintf('Min      %8.3f   %8.3f   %8.3f    %8.3f\n', ...
         min(YY(:,1)), min(YY(:,2)), min(YY(:,3)), min(YY(:,4)))
fprintf('Med      %8.3f   %8.3f   %8.3f    %8.3f\n', ...
         median(YY(:,1)), median(YY(:,2)), median(YY(:,3)), median(YY(:,4)))
fprintf('Max      %8.3f   %8.3f   %8.3f    %8.3f\n', ...
         max(YY(:,1)), max(YY(:,2)), max(YY(:,3)), max(YY(:,4)))

%% Plot
plot(co_chn_I_dBu);

