function [LRP2P_Aref, LRArea_Aref] = compute_LR_Aref_dB(tx, rx_loc)

LR_params.eps_dielet = 15.0;         % default
LR_params.sgm_conductivity = 0.005;  % default
LR_params.eno_ns_surfref = 301.0;    % default
LR_params.radio_climate = 'desert';
LR_params.timepct = 0.9;
LR_params.locpct = 0.5;
LR_params.confpct = 0.5;
LR_params.rht_m = 1.0;   % MWA receiver height

% The sample tx is located 181km from MWA
% tx = TV{1557};

start_point.latitude = tx.Lat;
start_point.longitude = tx.Lon;

end_point.latitude = rx_loc(1);
end_point.longitude = rx_loc(2);

LR_params.tht_m = tx.Height;
LR_params.frq_mhz = tx.FreqMHz;

if tx.Polar == 'H'|| tx.Polar == 'M' % for FM tx
    LR_params.pol = 0;
elseif tx.Polar == 'V'
    LR_params.pol = 1;
else
    tx;
    error('Unknown polarization parameter for DTV Tx');
end

[dBLoss1, LRP2P_Aref, ~, delta_H, ~] = LongleyRP2P(start_point, end_point, ...
    LR_params.tht_m, LR_params.rht_m, [], [], [], ...
    LR_params.frq_mhz, LR_params.radio_climate, LR_params.pol, LR_params.timepct,...
    LR_params.locpct, LR_params.confpct);

%% LRArea
% Sample:
% [dBLoss err dLOS_m Aref propa_param]=LongleyRArea( 10000, 3, 90, 200, 10.0, ...
%     2, 0, [], [], [], ...   % Set them to default
%     400.0, ...  % frequency in MHz; important
%     'continental temperate', 'horizontal', 0.5, 0.5, 0.5 );

[~, dist_m] = compute_dist(start_point.latitude, start_point.longitude, ...,
                           end_point.latitude, end_point.longitude);

%-------------------------------------------------------
% Note: there is a difference between delta_H computed by LongleyP2P and
% that computed by calcDeltaH(), but it does not have a significnat impact
% on the computed Aref for LRArea
                       
% [elev, ~] = loadElevationP2P(start_point, end_point);
% delta_H = calcDeltaH(elev);     

% fprintf('delta_H = %f\n', delta_H);
%-------------------------------------------------------

[dBLoss2, ~, ~, LRArea_Aref, ~] = LongleyRArea( dist_m, 3, delta_H, ...  % 3 - ModVar (Broadcast),
    LR_params.tht_m, LR_params.rht_m, ...
    2, 0, [], [], [], ...   % Set them to default
    LR_params.frq_mhz, ...  % frequency in MHz; important
    LR_params.radio_climate, LR_params.pol, ...
    LR_params.timepct, LR_params.locpct, LR_params.confpct);

% dBLoss2 - dBLoss1
% LRArea_Aref - LRP2P_Aref

%% Summary
fprintf('Tx dist = %f, delta_H = %f, LRP2P_Aref = %f, LRArea_Aref = %f\n', ...
         dist_m/1000, delta_H, LRP2P_Aref, LRArea_Aref);

