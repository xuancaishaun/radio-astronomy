function data_array = format_data(DAT_FILE, mode)

load(DAT_FILE);

N = length(AreaServed(:,1));

data_array = cell(N, 1);

if exist('tx', 'var')
    clear('tx');
end

for i = 1:N
    tx.Type = mode;
    
    tmp = AreaServed(i);
    tx.AreaServed = tmp{1};
    
    if strcmp(mode, 'AM') || strcmp(mode, 'TEM')
        tx.FreqMHz = FrequencykHz(i)/1000;
    else
        tx.FreqMHz = FrequencyMHz(i);
    end
    
    tmp = Purpose(i);
    tx.Purpose = tmp{1};
    
    tmp = Polarisation(i);
    tx.Polar = tmp{1};
    
    if strcmp(mode, 'AM')
        tx.Height = MastHeightm(i);
    elseif strcmp(mode, 'FM')
        tx.Height = AntennaHeightm(i);
    elseif strcmp(mode, 'DTV') || strcmp(mode, 'DR')
        tx.Height = AntennaHeight(i);
    else
        error('Cannot find antenna height');
    end
    
    if exist('AntennaPattern', 'var')
        tmp = AntennaPattern(i);
        tx.AntPattern = tmp{1};
    elseif exist('Pattern', 'var')
        tmp = Pattern(i);
        tx.AntPattern = tmp{1};
    else
        error('Cannot find antenna pattern');
    end
    
    if strcmp(mode, 'AM')
        tx.MaxCMFV = MaximumCMFV(i);
        tx.MaxERPW = TransmitterPowerW(i);   % Treat them as the same
    elseif strcmp(mode, 'FM') || strcmp(mode, 'DTV') || strcmp(mode, 'DR')
        tx.MaxCMFV = 0;
        tx.MaxERPW = MaximumERPW(i);
    else
        error('Cannot find (max) CMFV or ERPW');
    end
    
    tx.TechNum = TechnicalSpecificationNum(i);
    tx.LicNum = LicenceNumber(i);
    
    if exist('SiteId', 'var')
        tx.SiteID = SiteId(i);
    elseif exist('Siteid', 'var')
        tx.SiteID = Siteid(i);
    else
        error('Cannot find site ID');
    end
    
    tmp = SiteName(i);
    tx.SiteName = tmp{1};
    
    tx.Zone = Zone(i);
    tx.Easting = Easting(i);
    tx.Northing = Northing(i);
    
    % Convert Latitude
    lat = Latitude(i);
    sign = lat{1}(end);   % S - negative, N - positive
    vals = strsplit(lat{1}(1:end-1));
    deg = str2double(vals{1});
    min = str2double(vals{2});
    sec = str2double(vals{3});
    
    if strcmp(sign, 'S')
        lat_decimal = (deg + min/60 + sec/3600)*(-1);
    else
        lat_decimal = deg + min/60 + sec/3600;
    end
    
    tx.Lat = lat_decimal;
    
    % Convert Longtitude
    lon = Longitude(i);
    sign = lon{1}(end);
    vals = strsplit(lon{1}(1:end-1));
    deg = str2double(vals{1});
    min = str2double(vals{2});
    sec = str2double(vals{3});
    
    if strcmp(sign, 'W')
        lon_decimal = (deg + min/60 + sec/3600)*(-1);
    else
        lon_decimal = deg + min/60 + sec/3600;
    end
    
    tx.Lon = lon_decimal;
    
    tmp = State(i);
    tx.State = tmp{1};
    
    tx.BSL = BSL(i);
    
    data_array{i} = tx;
end