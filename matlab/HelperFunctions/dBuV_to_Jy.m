% This function converts a dBuV/m over a 7MHz channel to b Jy.
% 1 Jy (Jansky) = 10^(-26) W/m^2/Hz
function [b_Jy, b] = dBuV_to_Jy(a, mode)
if strcmp(mode, 'DTV')
    b = (10^(a/20)*10^(-6))^2/(377*7*10^6);
    b_Jy = b/(10^(-26));
elseif strcmp(mode, 'FM-HD')
    % With harmonic distortion
    b = (10^(a/20)*10^(-6))^2/(377*50*10^3)*0.003/100;
    b_Jy = b/(10^(-26));
end