% This function computes the received field strength in dBuV/m at the given
% receive location for the DTV signal from a given station.
% tx_data: DTV station
% rx_loc: [lat, lon]

function rss = compute_dtv_rss(tx_data, rx_loc, PL_model)

if isempty(PL_model)
    PL_model = 'FS';    % Free-space
end

ant_height = tx_data.Height;    % in meter
tx_lat = tx_data.Lat;
tx_lon = tx_data.Lon;

% First compute Free-Space path loss
% E(dBuV/m) = 106.92 + ERP(dBk) - 20 log d(km)

erp_dBk = 10 * log10(tx_data.MaxERPW/1000);
antenna_field_value = 1;        % Missing antenna field data
antenna_gain = 20*log10(antenna_field_value);
erp_dBk = erp_dBk + antenna_gain;

dist_km = compute_dist(tx_lat, tx_lon, rx_loc(1), rx_loc(2));

% Field strength for Free-Space model
fs_rss = 106.92 + erp_dBk - 20*log10(dist_km);    % in dBuV/m

%%
if strcmp(PL_model, 'FS')
    rss = fs_rss;
elseif strcmp(PL_model, 'TR')   % Two-Ray
    Aref = 20*log10(dist_km*1000) - 60 - 20*log10(ant_height);
    rss = fs_rss - Aref;
elseif strcmp(PL_model, 'LRP2P')
    [LRP2P_Aref, ~] = compute_LR_Aref_dB(tx_data, rx_loc);
    rss = fs_rss - LRP2P_Aref;
elseif strcmp(PL_model, 'LRArea')
    [~, LRArea_Aref] = compute_LR_Aref_dB(tx_data, rx_loc);
    rss = fs_rss - LRArea_Aref;
else
    warning('PL_model is not correct');
end