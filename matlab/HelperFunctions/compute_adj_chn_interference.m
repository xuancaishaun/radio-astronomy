% 0 <= f < 0.2 MHz
% f=0.004MHz * n, 0 <= n <= 49

E_linear = 0;

for i=0:50
   E_dB = -32.8 - 114.8571*(0.004*i + 0.15);
   E_linear = E_linear + 10^(E_dB/10);
end

% 0.2 <= f < 1.75
% f=0.004*n, 50 <= n <= 437
for i=50:438
   E_dB = -73 - 7.7419*(0.004*i - 0.2);
   E_linear = E_linear + 10^(E_dB/10);
end

% 1.75 <= f < 7
% f=0.004*n, 438 <= n < 1750
for i=438:1750
   E_dB = -85 - 4.7619*(0.004*i - 1.75);
   E_linear = E_linear + 10^(E_dB/10);
end

fprintf('Adjacent interference = %f\n', E_linear);

E_linear2 = 0;

for i=1750:3500
   E_dB2 = -110;
   E_linear2 = E_linear2 + 10^(E_dB2/10);
end

fprintf('Second-adjacent interference = %f\n', E_linear2);