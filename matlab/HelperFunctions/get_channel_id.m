function id_str = get_channel_id(freqMHz, mode)

if strcmp(mode, 'TV')
    freq_table = [140.5, 177.5, 184.5, 191.5, 198.5, 205.5, 212.5, 219.5, 226.5];
    chn_id_table = {'5A', '6',    '7',   '8',   '9',  '9A',  '10',  '11', '12'};

    [~, idx] = min(abs(freq_table - freqMHz));
    id_str = chn_id_table{idx};
    
elseif strcmp(mode, 'FM')
    freq_table = 88.1:0.2:107.9;
    chn_id_table = 1:length(freq_table);
    
    [~, idx] = min(abs(freq_table-freqMHz));
    id_str = sprintf('%d', chn_id_table(idx));
else
    error('Undefined mode');
end