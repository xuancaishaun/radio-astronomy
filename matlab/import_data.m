% Import TX parameters to Matlab
% Raw data files

if ~exist('Radio_Astronomy_Base','var')
    Radio_Astronomy_Base = '/home/shaun/Dropbox/MyResearch/xuhang.ying/radio_astronomy/matlab';
end

fprintf('Radio Astronomy Base = %s\n', Radio_Astronomy_Base);

DAT_AM_COL_VEC = [Radio_Astronomy_Base '/../dataset/raw_am_col_vec.mat'];
DAT_FM_COL_VEC = [Radio_Astronomy_Base '/../dataset/raw_fm_col_vec.mat'];
DAT_DR_COL_VEC = [Radio_Astronomy_Base '/../dataset/raw_dr_col_vec.mat'];
DAT_TV_COL_VEC = [Radio_Astronomy_Base '/../dataset/raw_dtv_col_vec.mat'];
DAT_TEM_COL_VEC = [Radio_Astronomy_Base '/../dataset/raw_tem_col_vec.mat'];

%% Create a structure for each station
% AM

if exist([Radio_Astronomy_Base '/../dataset/data/data_2016_Mar_10.mat'], 'file') == 2
    fprintf('Loading data in %s\n', [Radio_Astronomy_Base '/../dataset/data/data_2016_Mar_10.mat']);
    load([Radio_Astronomy_Base '/../dataset/data/data_2016_Mar_10.mat']);
else
    AM = format_data(DAT_AM_COL_VEC, 'AM');
    FM = format_data(DAT_FM_COL_VEC, 'FM');
    DR = format_data(DAT_DR_COL_/VEC, 'DR');     % ??
    TV = format_data(DAT_TV_COL_VEC, 'DTV');
    %TEM = format_data(DAT_TEM_COL_VEC, 'TEM');  % Temporary licenses
end

