Radio_Astronomy_Base = '~/Dropbox/MyResearch/xuhang.ying/radio_astronomy/matlab';
cd(Radio_Astronomy_Base);
addpath('HelperFunctions');
addpath('PlottingFunctions');

% Add TVWS files to the search path
addpath('~/Projects/TVWSSimulatorEngine');
set_path('~/Projects/TVWSSimulatorEngine');